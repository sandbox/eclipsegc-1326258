<?php

$plugin = array(
  'label' => t('Exportable Entity'),
  'get child' => 'ctools_entity_exportable_get_child',
  'get children' => 'ctools_entity_exportable_get_children',
);

function ctools_entity_exportable_get_child($plugin, $parent, $child) {
  $plugins = ctools_entity_exportable_get_children($plugin, $parent);
  return $plugins[$parent . ':' . $child];
}

function ctools_entity_exportable_get_children($plugin, $parent) {
  $plugins = array();
  if (db_table_exists('ctools_entity_entities')) {
    ctools_include('export');
    $entities = ctools_export_load_object('ctools_entity_entities');
    foreach ($entities as $entity_type => $entity) {
      $child_plugin = array();
      $child_plugin['label'] = t('@label', array('@label' => $entity->label));
      $child_plugin['base table'] = 'ctools_entity_' . $entity->machine_name;
      if ($entity->revisions) {
        $child_plugin['revision table'] = 'ctools_entity_' . $entity->machine_name . '_revision';
      }
      if ($entity->fieldable) {
        $child_plugin['fieldable'] = TRUE;
      }
      $plugins[$parent . ':' . $entity_type] = $child_plugin;
    }
  }

  return $plugins;
}
