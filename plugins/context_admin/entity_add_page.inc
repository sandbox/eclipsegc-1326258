<?php

$plugin = array(
  'title' => t('Entity Add Page'),
  'description' => t('Add an entity of a particular type.'),
  'content form' => 'ctools_entity_entity_add_page_content_form',
  'content form submit' => 'ctools_entity_entity_add_page_content_form_submit',
  'render' => 'ctools_entity_entity_add_page_render_page',
);

function ctools_entity_entity_add_page_content_form($form, &$form_state) {
  $options = array();
  ctools_include('plugins');
  $entity_plugins = ctools_get_plugins('ctools_entity', 'entities');
  foreach ($entity_plugins as $plugin_name => $plugin) {
    if (!isset($plugin['name'])) {
      $plugin['name'] = $plugin_name;
    }
    $class = ctools_entity_get_handler($plugin);
    if ($class && method_exists($class, 'hook_menu')) {
      $options[$class->entity_type] = $class->export->label;
    }
  }

  $form['entity_type'] = array(
    '#type' => 'select',
    '#title' => t('Entity Type'),
    '#description' => t('Select the type of entity you would like to create.'),
    '#options' => $options,
    '#default_value' => $form_state['conf']['entity_type'],
  );
  $form['bundle_admin'] = array(
    '#type' => 'textfield',
    '#title' => t('Bundle Administration URL'),
    '#default_value' => $form_state['conf']['bundle_admin'],
  );
  return $form;
}

function ctools_entity_entity_add_page_content_form_submit($form, &$form_state) {
  if (isset($form_state['values']['entity_type'])) {
    $form_state['conf']['entity_type'] = $form_state['values']['entity_type'];
  }
}

function ctools_entity_entity_add_page_render_page($handler, $contexts, $args, $test = TRUE) {
  $conf = $handler->conf;
  ctools_include('export');
  $content = ctools_export_load_object('ctools_entity_bundles', 'conditions', array('entity_type' => $conf['entity_type']));
  // Bypass the bundle listing if only one bundle is available.
  if (count($content) == 1) {
    $item = array_shift($content);
    drupal_goto(current_path() . '/' . $item->bundle);
  }
  return theme('entity_add_list', array('content' => $content, 'conf' => $conf));
}
