<?php

$plugin = array(
  'title' => t('Entity Add'),
  'description' => t('Create a new instance of a particular entity.'),
  'content form' => 'ctools_entity_entity_add_content_form',
  'content form submit' => 'ctools_entity_entity_add_content_form_submit',
  'render' => 'ctools_entity_entity_add_render_page',
);

function ctools_entity_entity_add_content_form($form, &$form_state) {
  $options = array();
  ctools_include('plugins');
  $entity_plugins = ctools_get_plugins('ctools_entity', 'entities');
  foreach ($entity_plugins as $plugin_name => $plugin) {
    if (!isset($plugin['name'])) {
      $plugin['name'] = $plugin_name;
    }
    $class = ctools_entity_get_handler($plugin);
    if ($class && method_exists($class, 'hook_menu')) {
      $options[$class->entity_type] = $class->export->label;
    }
  }

  $form['entity_type'] = array(
    '#type' => 'select',
    '#title' => t('Entity Type'),
    '#description' => t('Select the type of entity you would like to create.'),
    '#options' => $options,
    '#default_value' => $form_state['conf']['entity_type'],
    '#ajax' => array(
      'callback' => 'ctools_entity_entity_add_content_form_ajax',
      'wrapper' => 'entity_bundle',
    ),
  );
  if (!isset($form_state['values']['entity_type'])) {
    $form['bundle'] = array(
      '#type' => 'markup',
      '#prefix' => '<div id="entity_bundle">',
      '#suffix' => '</div>',
    );
  }
  else {
    $objects = ctools_export_load_object('ctools_entity_bundles', 'conditions', array('entity_type' => $form_state['values']['entity_type']));
    $bundle_options = array();
    foreach ($objects as $object) {
      $bundle_options[$object->machine_name] = $object->label;
    }
    $form['bundle'] = array(
      '#type' => 'select',
      '#title' => t('Bundle Administration URL'),
      '#default_value' => $form_state['conf']['bundle'],
      '#options' => $bundle_options,
      '#prefix' => '<div id="entity_bundle">',
      '#suffix' => '</div>',
    );
  }
  return $form;
}

function ctools_entity_entity_add_content_form_ajax($form, $form_state) {
  return $form['bundle'];
}

function ctools_entity_entity_add_content_form_submit($form, &$form_state) {
  if (isset($form_state['values']['entity_type'])) {
    $form_state['conf']['entity_type'] = $form_state['values']['entity_type'];
  }
  if (isset($form_state['values']['bundle'])) {
    $form_state['conf']['bundle'] = $form_state['values']['bundle'];
  }
}

function ctools_entity_entity_add_render_page($handler, $contexts, $args, $test = TRUE) {
  $type = $handler->conf['entity_type'];
  $info = entity_get_info($type);
  $values = array();
  // Assign the bundle value if given.
  if (isset($handler->conf['bundle'])) {
    $values[$info['entity keys']['bundle']] = $handler->conf['bundle'];
  }

  $entity = entity_create($type, $values);
  return entity_form($type, $entity);
}
