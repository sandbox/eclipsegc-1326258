<?php

/**
 * Base class for export UI.
 */
class ctools_entity_bundle extends ctools_export_ui {

  function hook_menu(&$items) {
    if (empty($this->plugin['schema'])) {
      return;
    }

    $entity_plugins = ctools_get_plugins('ctools_entity', 'entities');
    foreach ($entity_plugins as $plugin_name => $plugin) {
      if (!isset($plugin['name'])) {
        $plugin['name'] = $plugin_name;
      }
      $class = ctools_entity_get_handler($plugin);
      if ($class) {
        $prefix = $class->export->bundle_admin . '/' . $this->plugin['menu']['menu item'];
        if (isset($this->plugin['menu']['items']) && is_array($this->plugin['menu']['items'])) {
          $my_items = array();
          foreach ($this->plugin['menu']['items'] as $item) {
            // Add menu item defaults.
            $item += array(
              'file' => 'export-ui.inc',
              'file path' => drupal_get_path('module', 'ctools') . '/includes',
            );

            $path = !empty($item['path']) ? $prefix . '/' . $item['path'] : $prefix;
            unset($item['path']);
            $my_items[$path] = $item;
          }
          $items += $my_items;
        }
      }
    }
  }

  function get_entity_by_condition($value, $condition) {
    $entity = ctools_export_load_object('ctools_entity_entities', 'conditions', array($condition => $value));
    $key = array_keys($entity);
    return $entity[$key[0]];
  }

  // ------------------------------------------------------------------------
  // These methods are the API for generating the list of exportable items.

  /**
   * Master entry point for handling a list.
   *
   * It is unlikely that a child object will need to override this method,
   * unless the listing mechanism is going to be highly specialized.
   */
  function list_page($js, $input) {
    $path = explode('/', current_path());
    $last = array_pop($path);
    if ($last == $this->plugin['menu']['menu item']) {
      // Commenting out how this use to be populated.
      //$this->items = ctools_export_crud_load_all($this->plugin['schema'], $js);
      $entity = $this->get_entity_by_condition(implode('/', $path), 'bundle_admin');
      $this->items = ctools_export_load_object('ctools_entity_bundles', 'conditions', array('entity_type' => $entity->machine_name));
    }
    else {
      return;
    }

    // Respond to a reset command by clearing session and doing a drupal goto
    // back to the base URL.
    if (isset($input['op']) && $input['op'] == t('Reset')) {
      unset($_SESSION['ctools_export_ui'][$this->plugin['name']]);
      if (!$js) {
        return drupal_goto($_GET['q']);
      }
      // clear everything but form id, form build id and form token:
      $keys = array_keys($input);
      foreach ($keys as $id) {
        if (!in_array($id, array('form_id', 'form_build_id', 'form_token'))) {
          unset($input[$id]);
        }
      }
      $replace_form = TRUE;
    }

    // If there is no input, check to see if we have stored input in the
    // session.
    if (!isset($input['form_id'])) {
      if (isset($_SESSION['ctools_export_ui'][$this->plugin['name']]) && is_array($_SESSION['ctools_export_ui'][$this->plugin['name']])) {
        $input  = $_SESSION['ctools_export_ui'][$this->plugin['name']];
      }
    }
    else {
      $_SESSION['ctools_export_ui'][$this->plugin['name']] = $input;
      unset($_SESSION['ctools_export_ui'][$this->plugin['name']]['q']);
    }

    // This is where the form will put the output.
    $this->rows = array();
    $this->sorts = array();

    $form_state = array(
      'plugin' => $this->plugin,
      'input' => $input,
      'rerender' => TRUE,
      'no_redirect' => TRUE,
      'object' => &$this,
    );
    if (!isset($form_state['input']['form_id'])) {
      $form_state['input']['form_id'] = 'ctools_export_ui_list_form';
    }

    // If we do any form rendering, it's to completely replace a form on the
    // page, so don't let it force our ids to change.
    if ($js && isset($_POST['ajax_html_ids'])) {
      unset($_POST['ajax_html_ids']);
    }

    $form = drupal_build_form('ctools_export_ui_list_form', $form_state);
    $form = drupal_render($form);

    $output = $this->list_header($form_state) . $this->list_render($form_state) . $this->list_footer($form_state);

    if (!$js) {
      $this->list_css();
      return $form . $output;
    }

    $commands = array();
    $commands[] = ajax_command_replace('#ctools-export-ui-list-items', $output);
    if (!empty($replace_form)) {
      $commands[] = ajax_command_replace('#ctools-export-ui-list-form', $form);
    }
    print ajax_render($commands);
    ajax_footer();
  }

  // ------------------------------------------------------------------------
  // These methods are the API for adding/editing exportable items

  function add_page($js, $input, $step = NULL) {
    drupal_set_title($this->get_page_title('add'));

    // If a step not set, they are trying to create a new item. If a step
    // is set, they're in the process of creating an item.
    if (!empty($this->plugin['use wizard']) && !empty($step)) {
      $item = $this->edit_cache_get(NULL, 'add');
    }
    if (empty($item)) {
      $item = ctools_export_crud_new($this->plugin['schema']);
      $path = explode('/', current_path());
      $add = array_pop($path);
      $bundle = array_pop($path);
      if ($add == 'add' && $bundle == $this->plugin['menu']['menu item']) {
        $entity = $this->get_entity_by_condition(implode('/', $path), 'bundle_admin');
        $item->entity_type = $entity->machine_name;
      }
    }

    $form_state = array(
      'plugin' => $this->plugin,
      'object' => &$this,
      'ajax' => $js,
      'item' => $item,
      'op' => 'add',
      'form type' => 'add',
      'rerender' => TRUE,
      'no_redirect' => TRUE,
      'step' => $step,
      // Store these in case additional args are needed.
      'function args' => func_get_args(),
    );

    $output = $this->edit_execute_form($form_state);
    if (!empty($form_state['executed'])) {
      $export_key = $this->plugin['export']['key'];
      drupal_goto(str_replace('%ctools_export_ui', $form_state['item']->{$export_key}, $this->plugin['redirect']['add']));
    }

    return $output;
  }

  /**
   * Import form validate handler.
   *
   * Evaluates code and make sure it creates an object before we continue.
   */
  function edit_form_import_validate($form, &$form_state) {
    $item = ctools_export_crud_import($this->plugin['schema'], $form_state['values']['import']);
    if (is_string($item)) {
      form_error($form['import'], t('Unable to get an import from the code. Errors reported: @errors', array('@errors' => $item)));
      return;
    }

    $form_state['item'] = $item;
    $form_state['item']->export_ui_allow_overwrite = $form_state['values']['overwrite'];
    $form_state['item']->export_ui_code = $form_state['values']['import'];
  }
}
