<?php

$plugin = array(
  'schema' => 'ctools_entity_bundles',
  'access' => 'administer ctools_entity bundles',
  'menu' => array(
    'menu item' => 'bundle',
    'menu title' => 'Administer Entities', 
    'menu description' => 'Create and administer new entities.',
  ),
  'title singular' => t('bundle'),
  'title singular proper' => t('Bundle'),
  'title plural' => t('bundles'),
  'title plural proper' => t('Bundles'),
  'form' => array(
    'settings' => 'ctools_entity_bundle_export_ui_form',
    'validate' => 'ctools_entity_bundle_export_ui_form_validate',
    'submit' => 'ctools_entity_bundle_export_ui_form_submit',
  ),
  'handler' => array(
    'class' => 'ctools_entity_bundle',
  ),
);

function ctools_entity_bundle_export_ui_form(&$form, &$form_state) {
}

function ctools_entity_bundle_export_ui_form_validate(&$form, &$form_state) {
}

function ctools_entity_bundle_export_ui_form_submit(&$form, &$form_state) {
}
