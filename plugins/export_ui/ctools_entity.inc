<?php

$plugin = array(
  'schema' => 'ctools_entity_entities',
  'access' => 'administer ctools_entity entities',
  'menu' => array(
    'menu prefix' => 'admin/structure',
    'menu item' => 'ctools_entity',
    'menu title' => 'Administer Entities', 
    'menu description' => 'Create and administer new entities.',
  ),
  'title singular' => t('entity'),
  'title singular proper' => t('Entity'),
  'title plural' => t('entities'),
  'title plural proper' => t('Entities'),
  'form' => array(
    'settings' => 'ctools_entity_export_ui_form',
    'validate' => 'ctools_entity_export_ui_form_validate',
    'submit' => 'ctools_entity_export_ui_form_submit',
  ),
);

function ctools_entity_export_ui_form(&$form, &$form_state) {
  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#default_value' => $form_state['item']->label,
    '#required' => TRUE,
  );
  $form['fieldable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fieldable'),
    '#default_value' => $form_state['item']->fieldable,
  );
  $form['revisions'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Revisions'),
    '#default_value' => $form_state['item']->revisions,
  );
  $form['path_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Optional Path Settings'),
    '#collapsible' => TRUE,
    //'#collapsed' => TRUE,
  );
  $form['path_settings']['create_path_option'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Custom Create Path'),
    '#default_value' => !empty($form_state['item']->create_path),
  );
  $form['path_settings']['create_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Create Path'),
    '#default_value' => $form_state['item']->create_path,
    '#states' => array(
      'visible' => array(  // action to take.
        ':input[name="create_path_option"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['path_settings']['bundle_admin_option'] = array(
    '#type' => 'checkbox',
    '#title' => t('Bundle Administration'),
    '#default_value' => !empty($form_state['item']->bundle_admin),
  );
  $form['path_settings']['bundle_admin'] = array(
    '#type' => 'textfield',
    '#title' => t('Bundle Administration Path'),
    '#default_value' => $form_state['item']->bundle_admin,
    '#states' => array(
      'visible' => array(  // action to take.
        ':input[name="bundle_admin_option"]' => array('checked' => TRUE),
      ),
    ),
  );
}

function ctools_entity_export_ui_form_validate(&$form, &$form_state) {
  if (!isset($form_state['values']['fieldable'])) {
    $form_state['item']->fieldable = 0;
  }
  if (!isset($form_state['values']['revisions'])) {
    $form_state['item']->revisions = 0;
  }
  if ($form_state['values']['create_path_option'] && !$form_state['values']['create_path']) {
    form_set_error('create_path', t("You've opted to use a custom create path but have not provided one."));
  }
}

function ctools_entity_export_ui_form_submit(&$form, &$form_state) {
}
