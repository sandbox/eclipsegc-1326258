<?php

class ctoolsEntity {
  public $plugin = array();
  public $entity_type = '';
  public $export = array();

  function init($plugin) {
    ctools_include('export');
    $this->plugin = $plugin;
    $this->entity_type = $this->get_entity_type($plugin);
    $this->export = ctools_export_crud_load('ctools_entity_entities', $this->entity_type);
  }

  function get_entity_type($plugin) {
    list($plugin_name, $entity_type) = explode(':', $plugin['name']);
    return $entity_type;
  }

  function definition() {
    $defaults = array(
      'controller class' => 'EntityAPIController',
      'access callback' => 'entity_access',
      'creation callback' => 'entity_create',
      'save callback' => 'entity_save',
      'deletion callback' => 'entity_delete',
      'view callback' => 'entity_view',
    );

    $return = $this->plugin;
    $return += $defaults;

    $return['entity keys'] = $this->entity_keys($this->entity_type);
    $return['bundle keys'] = $this->bundle_keys($this->entity_type);
    $return['bundles'] = $this->bundles($this->entity_type);
    $return['view modes'] = $this->view_modes($this->entity_type);

    return $return;
  }

  function entity_keys($entity_type) {
    return array(
      'id' => 'id', 
      'revision' => 'rid', 
      'bundle' => 'type', 
      'label' => 'title',
    );
  }

  function bundle_keys($entity_type) {
    return array(
      'bundle' => 'type',
    );
  }

  function bundles($entity_type) {
    $return = array();
    ctools_include('export');
    foreach (ctools_export_load_object('ctools_entity_bundles', 'conditions', array('entity_type' => $entity_type)) as $bundle => $info) {
      //drupal_set_message('<pre>' . var_export($bundle, TRUE) . '</pre>');
      //drupal_set_message('<pre>' . var_export($info, TRUE) . '</pre>');
      /*$return[$type] = array(
        'label' => $name,
        'admin' => array(
          'path' => 'admin/entities/manage/%ctools_entity/%ctools_entity_bundle',
          'real path' => 'admin/entities/manage/' . str_replace('_', '-', $entity_type) . '/' . str_replace('_', '-', $type),
          'bundle argument' => 4,
          'access arguments' => array('administer ctools entity bundles'),
        ),
      );*/
    }
    return $return;
  }

  function view_modes($entity_type) {
    return array();
  }

  function hook_permission(&$perms) {
    $perms['create ' . $this->entity_type] = array(
      'title' => t('Create @entity', array('@entity' => $this->export->label)), 
    );
  }

  function hook_menu(&$items) {
    if ($this->export->create_path) {
      $items[$this->export->create_path] = $this->create_path();
    }
  }

  function create_path() {
    return array(
      'title' => 'Create '. $this->export->label,
      'page callback' => 'ctools_entity_entity_create_page',
      'page arguments' => array($this->entity_type, $this->export),
      'access arguments' => array('create ' . $this->entity_type),
      'type' => MENU_CALLBACK,
    );
  }

  function hook_theme(&$themes) {
    $themes['entity_add_list'] = array(
      'variables' => array('content' => NULL, 'conf' => NULL), 
      'file' => 'includes/ctools_entity.theme.inc',
    );
  }
}