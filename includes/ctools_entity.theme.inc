<?php

function theme_entity_add_list($vars) {
  $content = $vars['content'];
  $conf = $vars['conf'];
  $output = '';

  if ($content) {
    $output = '<dl class="entity-type-list">';
    foreach ($content as $item) {
      $output .= '<dt>' . l($item->label, current_path() . '/' . $item->bundle) . '</dt>';
    }
    $output .= '</dl>';
  }
  else {
    $output = '<p>' . t('You have not created any bundles yet. Go to the <a href="@create-bundle">bundle creation page</a> to add a new bundle.', array('@create-bundle' => url($conf['bundle_admin']))) . '</p>';
  }
  return $output;
}